using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Threading;
using System.IO;
using System.Drawing;
using System.Net.Sockets;


namespace UDPclientWPF
{

    public partial class MainWindow : Window
    {

        const int tamanhoBuffer = 1024;
        String path;

        Socket canal = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        Socket Tmsg = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        public MainWindow()
        {
            InitializeComponent();

        }

        private void Path(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDirectory = new OpenFileDialog();
            Nullable<bool> result = openDirectory.ShowDialog();
            if (result == true)
            {
                // Open document 
                string filename = openDirectory.FileName;
                txtPath.Text = filename;
                path = filename;
            }
        }

        private void send(object sender, RoutedEventArgs e)
        {
            blockScreen();

            byte[] sendbuf = new Byte[tamanhoBuffer];

            IPAddress broadcast = IPAddress.Parse("127.0.0.1");
            IPEndPoint ep = new IPEndPoint(broadcast, 8090);

            byte[] fileInput = File.ReadAllBytes(path);

            FileStream abcd = File.Open(path, FileMode.Open);
            var meuArquivo = new MemoryStream();
            abcd.CopyTo(meuArquivo);
            var bytes = meuArquivo.ToArray();
            int blocks = (bytes.Length) / tamanhoBuffer;
            byte[] a = Encoding.ASCII.GetBytes("de " + blocks);

            int aux = 0;
            int interno = 0;
            int cont = 0;
            int qtdBytesEnviados = 0;

            for (int i = 0; i < bytes.Length; i++)
            {
                if (aux < tamanhoBuffer)
                {
                    sendbuf[interno] = bytes[i];
                    aux++;
                    interno++;
                    qtdBytesEnviados++;
                }
                else
                {
                    canal.SendTo(sendbuf, ep);
                    Thread.Sleep(1);
                    cont++;
                    String printText = ("Mandei: " + cont + " Pacotes de " + blocks + "\n");
                    ProgressBarUDP(blocks, cont);
                    txtBook.Text = printText;
                    aux = 0;
                    interno = 0;
                    i--;
                }
            }
            blockScreen();
        }

        public void ProgressBarUDP(int fullBlocks, int countNow) 
        {
            float perc = (100 * countNow) / fullBlocks;
            progBar.Value = Math.Abs(perc);
            Thread.Sleep(1);
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            canal.Close();
            Tmsg.Close();
            unblockScreen();
        }

        public void blockScreen() {
            txtPath.IsEnabled = false;
            btn_Search.IsEnabled = false;
            btn_send.IsEnabled = false;
        }

        public void unblockScreen() {
            txtPath.IsEnabled = true;
            btn_Search.IsEnabled = true;
            btn_send.IsEnabled = true;
        }
    }
}
